﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UDBPortalRESTful.Models;
using UDBPortalRESTful.ViewModel;

namespace UDBPortalRESTful.Repository
{
    public interface IAreaRepository
    {
        Task<List<Area>> GetAreas();
        Task<AreaViewModel> GetArea(int? IdArea);
        Task<int> AddArea(Area area);
        Task<int> DeleteArea(int? IdArea);
        Task UpdateArea(Area area);
    }
}
