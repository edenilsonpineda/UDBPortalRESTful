﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UDBPortalRESTful.Models;
using UDBPortalRESTful.ViewModel;

namespace UDBPortalRESTful.Repository
{
    public class AreaRepository : IAreaRepository
    {
        UDBContext udbContext;
        public AreaRepository(UDBContext udbContext)
        {
            this.udbContext = udbContext;
        }

        public async Task<int> AddArea(Area area)
        {
            if (udbContext != null)
            {
                await udbContext.Area.AddAsync(area);
                await udbContext.SaveChangesAsync();

                return area.IdArea;
            }

            return 0;
        }

        public async Task<int> DeleteArea(int? areaId)
        {
            int result = 0;
            if (udbContext != null)
            {
                var area = await udbContext.Area.FirstOrDefaultAsync(x => x.IdArea == areaId);
                if (area != null)
                {
                    udbContext.Area.Remove(area);

                    result = await udbContext.SaveChangesAsync();
                }

                return result;
            }

            return 0;
        }

        public async Task<AreaViewModel> GetArea(int? areaId)
        {
            if(udbContext != null)
            {
                return await (from a in udbContext.Area
                              where a.IdArea == areaId
                              select new AreaViewModel
                              {
                                  IdArea = a.IdArea,
                                  IdDepto = a.IdDepto
                              }).FirstOrDefaultAsync();
            }

            return null;
        }

        public async Task<List<Area>> GetAreas()
        {
            if(udbContext != null)
            {
                return await udbContext.Area.ToListAsync();
            }

            return null;
        }

        public async Task UpdateArea(Area area)
        {
            if(udbContext != null)
            {
                udbContext.Area.Update(area);


                //commit the transaction
                await udbContext.SaveChangesAsync();
            }
        }
    }
}
