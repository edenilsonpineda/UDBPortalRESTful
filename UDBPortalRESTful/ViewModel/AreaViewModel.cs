﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UDBPortalRESTful.Models;

namespace UDBPortalRESTful.ViewModel
{
    public class AreaViewModel
    {
        public int IdArea { get; set; }
        public int IdDepto { get; set; }
        public string Area1 { get; set; }

    }
}
