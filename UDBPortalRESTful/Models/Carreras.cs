﻿using System;
using System.Collections.Generic;

namespace UDBPortalRESTful.Models
{
    public partial class Carreras
    {
        public Carreras()
        {
            AsignaturaCarrera = new HashSet<AsignaturaCarrera>();
        }

        public int IdCarrera { get; set; }
        public string Carrera { get; set; }

        public virtual ICollection<AsignaturaCarrera> AsignaturaCarrera { get; set; }
    }
}
