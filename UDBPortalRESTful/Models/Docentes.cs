﻿using System;
using System.Collections.Generic;

namespace UDBPortalRESTful.Models
{
    public partial class Docentes
    {
        public Docentes()
        {
            AsignaturaDocente = new HashSet<AsignaturaDocente>();
            Horarios = new HashSet<Horarios>();
        }

        public int IdDocente { get; set; }
        public int IdArea { get; set; }
        public string Docente { get; set; }
        public string Cubiculo { get; set; }

        public virtual ICollection<AsignaturaDocente> AsignaturaDocente { get; set; }
        public virtual ICollection<Horarios> Horarios { get; set; }
    }
}
