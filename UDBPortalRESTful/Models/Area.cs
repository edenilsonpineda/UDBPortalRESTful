﻿using System;
using System.Collections.Generic;

namespace UDBPortalRESTful.Models
{
    public partial class Area
    {
        public Area()
        {
            Asignaturas = new HashSet<Asignaturas>();
        }

        public int IdArea { get; set; }
        public int IdDepto { get; set; }
        public string Area1 { get; set; }

        public virtual Departamentos IdDeptoNavigation { get; set; }
        public virtual ICollection<Asignaturas> Asignaturas { get; set; }
    }
}
