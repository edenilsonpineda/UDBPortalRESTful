﻿using System;
using System.Collections.Generic;

namespace UDBPortalRESTful.Models
{
    public partial class Horarios
    {
        public int IdHorarios { get; set; }
        public int IdDocente { get; set; }
        public string Dia { get; set; }
        public string Hora { get; set; }

        public virtual Docentes IdDocenteNavigation { get; set; }
    }
}
