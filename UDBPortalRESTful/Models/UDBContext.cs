﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace UDBPortalRESTful.Models
{
    public partial class UDBContext : DbContext
    {
        public UDBContext()
        {
        }

        public UDBContext(DbContextOptions<UDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Area> Area { get; set; }
        public virtual DbSet<AsignaturaCarrera> AsignaturaCarrera { get; set; }
        public virtual DbSet<AsignaturaDocente> AsignaturaDocente { get; set; }
        public virtual DbSet<Asignaturas> Asignaturas { get; set; }
        public virtual DbSet<Carreras> Carreras { get; set; }
        public virtual DbSet<Departamentos> Departamentos { get; set; }
        public virtual DbSet<Docentes> Docentes { get; set; }
        public virtual DbSet<Horarios> Horarios { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (!optionsBuilder.IsConfigured)
        //    {
        //        optionsBuilder.UseSqlServer("Server=DESKTOP-KRQE9QR\\SQLEXPRESS;Database=UDB;User=SA;Password=Papromptw0rd;Trusted_Connection=true");
        //    }
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Area>(entity =>
            {
                entity.HasKey(e => e.IdArea)
                    .HasName("PK__AREA__750ECEA4BB06C591");

                entity.ToTable("AREA");

                entity.Property(e => e.IdArea).HasColumnName("idArea");

                entity.Property(e => e.Area1)
                    .IsRequired()
                    .HasColumnName("Area")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.IdDepto).HasColumnName("idDepto");

                entity.HasOne(d => d.IdDeptoNavigation)
                    .WithMany(p => p.Area)
                    .HasForeignKey(d => d.IdDepto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Departamento_Area");
            });

            modelBuilder.Entity<AsignaturaCarrera>(entity =>
            {
                entity.HasKey(d => new { d.CodAsignatura, d.IdCarrera });

                entity.ToTable("ASIGNATURA_CARRERA");

                entity.Property(e => e.IdCarrera).HasColumnName("idCarrera");

                entity.HasOne(d => d.CodAsignaturaNavigation)
                    .WithMany(p => p.AsignaturaCarrera)
                    .HasForeignKey(d => d.CodAsignatura)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Asignatura_Carrerauno");

                entity.HasOne(d => d.IdCarreraNavigation)
                    .WithMany(p => p.AsignaturaCarrera)
                    .HasForeignKey(d => d.IdCarrera)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Asignatura_Carrerados");
            });

            modelBuilder.Entity<AsignaturaDocente>(entity =>
            {
                entity.HasKey(d => new { d.CodAsignatura, d.IdDocente });

                entity.ToTable("ASIGNATURA_DOCENTE");

                entity.Property(e => e.IdDocente).HasColumnName("idDocente");

                entity.HasOne(d => d.CodAsignaturaNavigation)
                    .WithMany(p => p.AsignaturaDocente)
                    .HasForeignKey(d => d.CodAsignatura)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Asignatura_Docenteuno");

                entity.HasOne(d => d.IdDocenteNavigation)
                    .WithMany(p => p.AsignaturaDocente)
                    .HasForeignKey(d => d.IdDocente)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Asignatura_Docentedos");
            });

            modelBuilder.Entity<Asignaturas>(entity =>
            {
                entity.HasKey(e => e.CodAsignatura)
                    .HasName("PK__ASIGNATU__081CA026E90FF706");

                entity.ToTable("ASIGNATURAS");

                entity.Property(e => e.Asignatura)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Duracion)
                    .IsRequired()
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.IdArea).HasColumnName("idArea");

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAreaNavigation)
                    .WithMany(p => p.Asignaturas)
                    .HasForeignKey(d => d.IdArea)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Area_Asignatura");
            });

            modelBuilder.Entity<Carreras>(entity =>
            {
                entity.HasKey(e => e.IdCarrera)
                    .HasName("PK__CARRERAS__7B19E791EFA05FB3");

                entity.ToTable("CARRERAS");

                entity.Property(e => e.IdCarrera).HasColumnName("idCarrera");

                entity.Property(e => e.Carrera)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Departamentos>(entity =>
            {
                entity.HasKey(e => e.IdDepto)
                    .HasName("PK__DEPARTAM__F1B8249BA52BE868");

                entity.ToTable("DEPARTAMENTOS");

                entity.Property(e => e.IdDepto).HasColumnName("idDepto");

                entity.Property(e => e.Departamento)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Docentes>(entity =>
            {
                entity.HasKey(e => e.IdDocente)
                    .HasName("PK__DOCENTES__595F5B9C74D92DAD");

                entity.ToTable("DOCENTES");

                entity.Property(e => e.IdDocente).HasColumnName("idDocente");

                entity.Property(e => e.Cubiculo)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Docente)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.IdArea).HasColumnName("idArea");
            });

            modelBuilder.Entity<Horarios>(entity =>
            {
                entity.HasKey(e => e.IdHorarios)
                    .HasName("PK__HORARIOS__D5308F9BD50BA84F");

                entity.ToTable("HORARIOS");

                entity.Property(e => e.IdHorarios).HasColumnName("idHorarios");

                entity.Property(e => e.Dia)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Hora)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IdDocente).HasColumnName("idDocente");

                entity.HasOne(d => d.IdDocenteNavigation)
                    .WithMany(p => p.Horarios)
                    .HasForeignKey(d => d.IdDocente)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Docente_Horariosuno");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
