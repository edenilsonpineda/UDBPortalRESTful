﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace UDBPortalRESTful.Models
{
    public partial class AsignaturaCarrera
    {
        public int CodAsignatura { get; set; }
        public int IdCarrera { get; set; }

        
        public virtual Asignaturas CodAsignaturaNavigation { get; set; }
        
        public virtual Carreras IdCarreraNavigation { get; set; }
    }
}
