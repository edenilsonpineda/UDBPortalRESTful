﻿using System;
using System.Collections.Generic;

namespace UDBPortalRESTful.Models
{
    public partial class Asignaturas
    {
        public Asignaturas()
        {
            AsignaturaCarrera = new HashSet<AsignaturaCarrera>();
            AsignaturaDocente = new HashSet<AsignaturaDocente>();
        }

        public int CodAsignatura { get; set; }
        public int IdArea { get; set; }
        public string Asignatura { get; set; }
        public string Tipo { get; set; }
        public int Curso { get; set; }
        public double CreditosT { get; set; }
        public double CreditosL { get; set; }
        public string Duracion { get; set; }
        public int GruposT { get; set; }
        public int GruposL { get; set; }

        public virtual Area IdAreaNavigation { get; set; }
        public virtual ICollection<AsignaturaCarrera> AsignaturaCarrera { get; set; }
        public virtual ICollection<AsignaturaDocente> AsignaturaDocente { get; set; }
    }
}
