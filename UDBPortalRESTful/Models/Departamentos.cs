﻿using System;
using System.Collections.Generic;

namespace UDBPortalRESTful.Models
{
    public partial class Departamentos
    {
        public Departamentos()
        {
            Area = new HashSet<Area>();
        }

        public int IdDepto { get; set; }
        public string Departamento { get; set; }

        public virtual ICollection<Area> Area { get; set; }
    }
}
