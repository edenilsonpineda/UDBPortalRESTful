﻿using System;
using System.Collections.Generic;

namespace UDBPortalRESTful.Models
{
    public partial class AsignaturaDocente
    {
        public int CodAsignatura { get; set; }
        public int IdDocente { get; set; }

        public virtual Asignaturas CodAsignaturaNavigation { get; set; }
        public virtual Docentes IdDocenteNavigation { get; set; }
    }
}
