﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UDBPortalRESTful.Models;

namespace UDBPortalRESTful.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AsignaturasController : ControllerBase
    {
        private readonly UDBContext _context;

        public AsignaturasController(UDBContext context)
        {
            _context = context;
        }

        // GET: api/Asignaturas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Asignaturas>>> GetAsignaturas()
        {
            return await _context.Asignaturas.ToListAsync();
        }

        // GET: api/Asignaturas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Asignaturas>> GetAsignaturas(int id)
        {
            var asignaturas = await _context.Asignaturas.FindAsync(id);

            if (asignaturas == null)
            {
                return NotFound();
            }

            return asignaturas;
        }

        // PUT: api/Asignaturas/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsignaturas(int id, Asignaturas asignaturas)
        {
            if (id != asignaturas.CodAsignatura)
            {
                return BadRequest();
            }

            _context.Entry(asignaturas).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AsignaturasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Asignaturas
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Asignaturas>> PostAsignaturas(Asignaturas asignaturas)
        {
            _context.Asignaturas.Add(asignaturas);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAsignaturas", new { id = asignaturas.CodAsignatura }, asignaturas);
        }

        // DELETE: api/Asignaturas/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Asignaturas>> DeleteAsignaturas(int id)
        {
            var asignaturas = await _context.Asignaturas.FindAsync(id);
            if (asignaturas == null)
            {
                return NotFound();
            }

            _context.Asignaturas.Remove(asignaturas);
            await _context.SaveChangesAsync();

            return asignaturas;
        }

        private bool AsignaturasExists(int id)
        {
            return _context.Asignaturas.Any(e => e.CodAsignatura == id);
        }
    }
}
